/* util */
function js_strto_time(str_time) {
  var new_str = str_time.replace(/:/g, '-');
  new_str = new_str.replace(/ /g, '-');
  var arr = new_str.split("-");
  var strtotime = 0;
  var time = 0;
  var datum = new Date(Date.UTC(arr[0], arr[1] - 1, arr[2], arr[3] - 8, arr[4], arr[5]));
  if (datum != null && typeof datum != 'undefined') {
    time = datum.getTime();
  }
  return time;
}
window.onerror = function() {
    alert("Error caught");
};
function js_datestr_to_time(str_time) {
  var new_str = str_time.replace(/:/g, '-');
  new_str = new_str.replace(/ /g, '-');
  var arr = new_str.split("-");
  var strtotime = 0;
  var time = 0;
  var datum = new Date(Date.UTC(arr[0], arr[1] - 1, arr[2], -8, 0, 0));
  if (datum != null && typeof datum != 'undefined') {
    time = datum.getTime();
  }
  return time;
}
/**
 * 时间戳转日期
 * @Author: xuguanlong
 * @param   {[type]}   unixtime [时间戳]
 * @return  {[type]}            [时间戳对应的日期]
 */
function js_date_time(unixtime) {
  var timestr = new Date(parseInt(unixtime));
  var datetime = date_format(timestr, 'yyyy-MM-dd hh:mm:ss');
  return datetime;
}
/**
 * 日期格式化  yyyy-MM-dd hh:mm:ss
 * @Author: xuguanlong
 * @param   {[type]}   date [description]
 * @return  {[type]}        [description]
 */
function date_format(date, format) {
  var o = {
    "M+": date.getMonth() + 1, //month
    "d+": date.getDate(), //day
    "h+": date.getHours(), //hour
    "m+": date.getMinutes(), //minute
    "s+": date.getSeconds(), //second
    "q+": Math.floor((date.getMonth() + 3) / 3), //quarter
    "S": date.getMilliseconds() //millisecond
  }
  if (/(y+)/.test(format)) {
    format = format.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
  }

  for (var k in o) {
    if (new RegExp("(" + k + ")").test(format)) {
      format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
    }
  }
  return format;
}

$.fn.serializeObject = function() {
  var o = {};
  var a = this.serializeArray();
  $.each(a, function() {
    if (o[this.name]) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
      }
      o[this.name].push(this.value || '');
    } else {
      o[this.name] = this.value || '';
    }
  });
  return o;
};

$(function() {
  activeNavByURL();
  initThriftClient();

  function activeNavByURL() {
    var pathname = location.pathname;
    var router = pathname.slice(pathname.lastIndexOf('/'))

    $('.nav a').filter(function(index, val) {
      var href = $(val).attr('href');
      var res = router.indexOf(href);
      return res == -1 ? false : true;
    }).parent().addClass('active');
  }

  function initThriftClient() {
    var transport = new Thrift.Transport("http://120.55.119.181:9999/transportService", {"useCORS": true});
    var transport1 = new Thrift.Transport("http://120.55.119.181:9999/contentService", {"useCORS": true});
    var transport2 = new Thrift.Transport("http://120.55.119.181:9999/userService", {"useCORS": true});

    var protocol = new Thrift.Protocol(transport);
    client = new TruckTrack.TTransportServiceClient(protocol, protocol);
    /*
     通知内容修改
    */
    var protocol1 = new Thrift.Protocol(transport1);
    contentClient = new Content.TContentServiceClient(protocol1, protocol1);

    /*
     用户登录
    */
    var protocol2 = new Thrift.Protocol(transport2);
    userClient = new User.TUserServiceClient(protocol2, protocol2);

    userClient
      .login('aaa', 'bbb', function(e) {
        console.log('success cb');
      })
      .fail(function(e){console.log(e, 'finally')});
  }
});
