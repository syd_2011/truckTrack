//
// Autogenerated by Thrift Compiler (0.9.3)
//
// DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
//


if (typeof Geo === 'undefined') {
  Geo = {};
}
Geo.TPoint = function(args) {
  this.lat = null;
  this.lng = null;
  if (args) {
    if (args.lat !== undefined && args.lat !== null) {
      this.lat = args.lat;
    } else {
      throw new Thrift.TProtocolException(Thrift.TProtocolExceptionType.UNKNOWN, 'Required field lat is unset!');
    }
    if (args.lng !== undefined && args.lng !== null) {
      this.lng = args.lng;
    } else {
      throw new Thrift.TProtocolException(Thrift.TProtocolExceptionType.UNKNOWN, 'Required field lng is unset!');
    }
  }
};
Geo.TPoint.prototype = {};
Geo.TPoint.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.DOUBLE) {
        this.lat = input.readDouble().value;
      } else {
        input.skip(ftype);
      }
      break;
      case 2:
      if (ftype == Thrift.Type.DOUBLE) {
        this.lng = input.readDouble().value;
      } else {
        input.skip(ftype);
      }
      break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

Geo.TPoint.prototype.write = function(output) {
  output.writeStructBegin('TPoint');
  if (this.lat !== null && this.lat !== undefined) {
    output.writeFieldBegin('lat', Thrift.Type.DOUBLE, 1);
    output.writeDouble(this.lat);
    output.writeFieldEnd();
  }
  if (this.lng !== null && this.lng !== undefined) {
    output.writeFieldBegin('lng', Thrift.Type.DOUBLE, 2);
    output.writeDouble(this.lng);
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

Geo.TPolygon = function(args) {
  this.points = null;
  if (args) {
    if (args.points !== undefined && args.points !== null) {
      this.points = Thrift.copyList(args.points, [Geo.TPoint]);
    } else {
      throw new Thrift.TProtocolException(Thrift.TProtocolExceptionType.UNKNOWN, 'Required field points is unset!');
    }
  }
};
Geo.TPolygon.prototype = {};
Geo.TPolygon.prototype.read = function(input) {
  input.readStructBegin();
  while (true)
  {
    var ret = input.readFieldBegin();
    var fname = ret.fname;
    var ftype = ret.ftype;
    var fid = ret.fid;
    if (ftype == Thrift.Type.STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
      if (ftype == Thrift.Type.LIST) {
        var _size0 = 0;
        var _rtmp34;
        this.points = [];
        var _etype3 = 0;
        _rtmp34 = input.readListBegin();
        _etype3 = _rtmp34.etype;
        _size0 = _rtmp34.size;
        for (var _i5 = 0; _i5 < _size0; ++_i5)
        {
          var elem6 = null;
          elem6 = new Geo.TPoint();
          elem6.read(input);
          this.points.push(elem6);
        }
        input.readListEnd();
      } else {
        input.skip(ftype);
      }
      break;
      case 0:
        input.skip(ftype);
        break;
      default:
        input.skip(ftype);
    }
    input.readFieldEnd();
  }
  input.readStructEnd();
  return;
};

Geo.TPolygon.prototype.write = function(output) {
  output.writeStructBegin('TPolygon');
  if (this.points !== null && this.points !== undefined) {
    output.writeFieldBegin('points', Thrift.Type.LIST, 1);
    output.writeListBegin(Thrift.Type.STRUCT, this.points.length);
    for (var iter7 in this.points)
    {
      if (this.points.hasOwnProperty(iter7))
      {
        iter7 = this.points[iter7];
        iter7.write(output);
      }
    }
    output.writeListEnd();
    output.writeFieldEnd();
  }
  output.writeFieldStop();
  output.writeStructEnd();
  return;
};

